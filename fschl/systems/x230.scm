(define-module (fschl systems x230)
  #:use-module (fschl utils)
  #:use-module (fschl systems base)
  #:use-module (fschl systems common)
;  #:use-module (fschl home-services pipewire)
  #:use-module (fschl home-services xsettingsd)
  #:use-module (gnu home)
  #:use-module (gnu packages file-systems)
  #:use-module (gnu services)
  #:use-module (gnu system)
  #:use-module (gnu system uuid)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system mapped-devices)
  #:use-module (nongnu packages linux))

(define home
  (home-environment
   (services (cons*
              (service home-xsettingsd-service-type
                       (home-xsettingsd-configuration
                        (dpi 180)))
;              (service home-pipewire-service-type)
              common-home-services))))

(define system
  (operating-system
   (inherit base-operating-system)
   (host-name "x230")

   ;; Add sof-firmware drivers for audio on ThinkPad x230
 ;;  (firmware (list linux-firmware sof-firmware))

;;           (source (uuid "ad42a856-8e7c-4844-a55a-23ba4bd03dcc"))
;;           (target "system-root")
;;           (type luks-device-mapping))))

   (file-systems (cons*
                  (file-system
                   (device "/dev/sda3")
                   (mount-point "/")
                   (type "ext4"))
                  %base-file-systems))))

;; Return home or system config based on environment variable
(if (getenv "RUNNING_GUIX_HOME") home system)
