(define-module (fschl systems tower-guix)
  #:use-module (fschl utils)
  #:use-module (fschl systems base)
  #:use-module (fschl systems common)
  ;; #:use-module (fschl home-services pipewire)
  #:use-module (fschl home-services xsettingsd)
  #:use-module (gnu)
  #:use-module (gnu home)
  #:use-module (gnu home services xdg)
  #:use-module (gnu home services sound)
  #:use-module (gnu packages file-systems)
  #:use-module (gnu services)
  #:use-module (gnu services docker)
  #:use-module (gnu system)
  #:use-module (gnu system uuid)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system mapped-devices)
  #:use-module (nongnu packages linux))

(define home
  (home-environment
   (services (cons*
              (service home-xsettingsd-service-type
                       (home-xsettingsd-configuration
                        (dpi 180)))
              ;; (service home-pipewire-service-type)
              (simple-service
               'xdg-user-directories-config-service
               home-xdg-user-directories-service-type
               (home-xdg-user-directories-configuration
                (desktop     "$HOME/desktop")
                (documents   "/media/store/Documents")
                (download    "/media/store/Downloads")
                (music       "/media/store/Music")
                (pictures    "/media/store/Pictures")
                (publicshare "/media/store/Public")
                (templates   "/media/store/Templates")
                (videos      "/media/store/Videos")))
              common-home-services))))

(define system
  (operating-system
   (inherit base-operating-system)
   (host-name "tower-guix")

   (keyboard-layout (keyboard-layout "de" "altgr-intl" #:options '("ctrl:nocaps")))
   (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets '("/boot/efi"))
                (keyboard-layout keyboard-layout)))

   (file-systems (cons*
                  (file-system
                   (device "/dev/nvme0n1p3")
                   (mount-point "/")
                   (type "ext4"))
                  (file-system
                   (device "/dev/sdd1")
                   (mount-point "/media/store")
                   (type "ext4"))
                  (file-system
                   (device "/dev/sda1")
                   (mount-point "/media/docs")
                   (type "ext4"))
                  (file-system
                   ;; (device "/dev/nvme0n1p1")
                   (device (uuid "7241-D6CA" 'fat))
                   (mount-point "/boot/efi")
                   (type "vfat"))
                  %base-file-systems))))

;; Return home or system config based on environment variable
(if (getenv "RUNNING_GUIX_HOME") home system)
