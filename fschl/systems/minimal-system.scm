;; This is an operating system configuration template
;; for a "bare bones" setup, with no X11 display server.
(define-module (fschl systems minimal-system)
  #:use-module (srfi srfi-1)
  #:use-module (gnu)
  #:use-module (gnu services docker)
  #:use-module (gnu system)
  #:use-module (gnu system nss)
  #:use-module (gnu system setuid))

(use-modules (gnu))
(use-service-modules networking
                     ;; dbus ; required by dockerd
                     docker
                     guix virtualization ssh)
(use-package-modules linux
                     docker
                     ;; freedesktop ; required by elogind, which is required by dockerd
                     screen ssh vim certs)


;; (define-public base-operating-system
(operating-system
  (host-name "home-server")
  (timezone "Europe/Berlin")
  (locale "en_US.utf8")

  (keyboard-layout (keyboard-layout "de" "altgr-intl" #:model "thinkpad"))

  ;; Boot in "legacy" BIOS mode, assuming /dev/sdX is the
  ;; target hard disk, and "my-root" is the label of the target
  ;; root file system.
  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (targets '("/dev/sdX"))))
  ;; It's fitting to support the equally bare bones ‘-nographic’
  ;; QEMU option, which also nicely sidesteps forcing QWERTY.
  (kernel-arguments (list "console=ttyS0,115200"))
  (file-systems (cons (file-system
                        (device (file-system-label "my-root"))
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))

  ;; This is where user accounts are specified.  The "root"
  ;; account is implicit, and is initially created with the
  ;; empty password.
  (users (cons (user-account
                (name "fschl")
                (comment "F S")
                (group "users")

                ;; Adding the account to the "wheel" group makes it a
                ;; sudoer.
                (supplementary-groups '("wheel"
                                        "netdev"
                                        ;; "docker"
                                        )))
               %base-user-accounts))

  ;; Globally-installed packages.
  (packages (cons* vim
                   net-tools
                   %base-packages))

  ;; Add services to the baseline: a DHCP client and
  ;; an SSH server.
  (services (append (list (service dhcp-client-service-type) 
                          ;; (service dbus-root-service-type) ;; required by dockerd
                          ;; (service elogind-service-type) ;; required by dockerd
                          ;; (service docker-service-type)
                          ;; (service oci-container-service-type
                          ;;          (list
                          ;;           (oci-container-configuration
                          ;;            (image "jellyfin/jellyfin")
                          ;;            (provision "jellyfin")
                          ;;            (network "host")
                          ;;            (ports
                          ;;             '(("8096" . "8096")))
                          ;;            (volumes
                          ;;             '("jellyfin-config:/config"
                          ;;               "jellyfin-cache:/cache"
                          ;;               "/home/daviwil/Media:/media")))))
                          ;; (service oci-container-service-type
                          ;;          (list
                          ;;           (oci-container-configuration
                          ;;            (image "pihole/pihole:latest")
                          ;;            (provision "pihole")
                          ;;            (network "host")
                          ;;            (ports
                          ;;             '("53:53/tcp"
                          ;;               "53:53/udp"
                          ;;               "67:67/udp"  ; Only required if using Pi-hole as DHCP server
                          ;;               "80:80/tcp"))
                          ;;            (environment
                          ;;             '(("TZ" . "America/Chicago")))
                          ;;               ; Uncomment and set a secure password, or it will be random
                          ;;               ; '(("WEBPASSWORD" . "set a secure password here"))
                          ;;            (volumes
                          ;;             '("/docker/Pihole:/etc/pihole"
                          ;;               "/docker/Dnsmasq:/etc/dnsmasq.d")))))
                          (service openssh-service-type
                                   (openssh-configuration
                                    (openssh openssh-sans-x)
                                    (port-number 2222))))
                    %base-services)))


;; (base-operating-system)
