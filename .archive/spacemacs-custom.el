(setq org-roam-directory (file-truename "~/org-roam"))
(org-roam-db-autosync-mode)

(require 'org-projectile)

(eval-after-load 'ox '(require 'ox-koma-letter))
(eval-after-load 'ox-koma-letter
  '(progn
     (add-to-list 'org-latex-classes
                  '("Brief-de-modern"
                    "\\documentclass\[Brief-de-modern\]\{scrlttr2\}
     \[DEFAULT-PACKAGES]
     \[PACKAGES]
     \[EXTRA]"))

     ;; \\usepackage[english]{babel}
     ;; \\setkomavar{frombank}{(1234)\\,567\\,890}

     (setq org-koma-letter-default-class "Brief-de-modern")))

(setq org-default-notes-file "~/Documents/Org/tasks.org")
(setq org-agenda-files
      (quote ("~/Documents/Org/tasks.org"
              "~/Documents/Org/journal.org"
              "~/Documents/Org/private.org"
              "~/Documents/Org/projects.org"
              "~/Documents/Org/birthdays.org")))

(defun fschl/create-org-letter ()
  "Create a new letter in ~/Documents/letters/ with filename and date"
  (interactive)
  (let ((name (read-string "Filename: ")))
    (expand-file-name (format "%s.org" name) "~/Documents/letters/") ))

;; https://orgmode.org/manual/Template-elements.html
;; https://orgmode.org/manual/Template-expansion.html
(setq org-capture-templates
      '(("t" "todo list item" entry
         (file+olp+datetree "~/Documents/Org/tasks.org")
         "* TODO %?\n SCHEDULED: %^T"
         :tree-type month
         )
        ("T" "todo list item with source" entry
         (file+olp+datetree "~/Documents/Org/tasks.org")
         "* TODO %?\n %a \n SCHEDULED: %^T \n %^G \n"
         :tree-type month
         )

        ("r" "Todo research some website/software" entry
         (file+olp+datetree "~/Documents/Org/tasks.org")
         "* TODO %?\n  SCHEDULED: %^T \n %^L \n"
         :tree-type month
         )
        ("l" "letter to Documents/letters/<datetime.org>"
         entry (file fschl/create-org-letter)
         "* Preamble :noexport:
# #+TITLE: ?
# #+DATE:

#+SUBJECT: Betreff des Briefs

#+LCO: Absender-Frieder
# #+LCO: Absender-Marcelle
# #+LCO: Absender-FamilieSchlesier
#+LCO: Brief-de-modern
#+STARTUP: showall

* To-address :to:

# * From :from:

* Sehr geehrte Damen und Herren,
?

* Mit freundlichen Grüßen, :closing:

Frieder Schlesier"
         )
        ("m" "Schedule a meeting" entry
         (file+headline "~/Documents/Org/tasks.org")
         "* MEETING %?\n SCHEDULED: %^T\n %a"
         )

        ("p" "Schedule a phone call" entry
         (file+headline "~/Documents/Org/tasks.org")
         "* PHONE %?\n SCHEDULED: %^T\\n %a"
         )

        ("a" "Articles: keep notes of online articles"
         entry
         (file+datetree "~/Documents/Org/journal.org")
         "* %? \n%x \n %u\n- $?"
         :tree-type month
         :kill-buffer t
         :empty-lines-before 1)
        )
      )

(setq org-ref-default-bibliography '("~/Documents/references/references.bib")
      org-ref-pdf-directory "~/Documents/references/"
      org-ref-bibliography-notes "~/Documents/references/notes.org")
