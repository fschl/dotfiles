set encoding=utf8

set number
set ruler
set smartcase
set hlsearch
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4

set ai "auto indent
set si "smart indent
set wrap "wrap lines

syntax enable
