#!/bin/bash
set -e
set -x

# backup.sh

pre="/home/fschl"
target="/media/store/"
# target="/media/driveBay/Backups"

folders=( "Documents" #contains Org files
          "Downloads"
          "projects"
        )

folders_sensitive=( 
                    ".mozilla"
                    ".thunderbird"
                    ".ssh"
                    ".config/etc-tinc"
                  )

files=( 
        ".bash_history"
      )

main() {
    local cmd=$1

    if [[ -z "$cmd" ]]; then
        echo "Usage: \n std  | files | sensitive"
    fi

    case "$cmd" in
        std)
            for dir in "${folders[@]}";
            do
                if [ ! -d "${target}/${dir}" ]; then
                    echo "creating non-existing dir: ${target}/${dir}"
                    mkdir -p ${target}/$dir
                fi
                echo "${pre}/${dir} --> ${target}/${dir}"
                rsync --progress -ruv --ignore-existing "${pre}/${dir}" "${target}/"
            done
            ;;

        sensitive)
            for dir in "${folders_sensitive[@]}";
            do
                if [ ! -d "${target}/${dir}" ]; then
                    echo "creating non-existing dir: ${target}/dotdirectories/${dir}"
                    mkdir -p ${target}/dotdirectories/$dir
                fi
                echo "${pre}/${dir} --> ${target}/${dir}"
                rsync --progress -ruv --ignore-existing "${pre}/${dir}" "${target}/dotdirectories/"
            done
            ;;

    esac
}

main "$@" 2>&1 | tee "${pre}/backup_$(date +%Y-%m-%d_%H-%M)_$1.log"
