#!/bin/bash
set -e

# install.sh
#	This script installs a basic setup for a debian machine

USERNAME=fschl
SUDO="sudo"

check_root() {
    # We need root rights at some point
    if [ "$(whoami)" != "root" ]; then
      if ! which $SUDO >/dev/null; then
        echo "ERROR: $0 is not run as root and $SUDO is not available" >&2
        exit 1
      fi
    else
      SUDO="" # We're already root
    fi
}

base_applications() {
    echo "update and installing baseapps..."

    $SUDO pacman -Syu \
        bash-completion \
        bmon \
        bzip2 \
        ca-certificates \
        cmake \
        coreutils \
        cryptsetup \
        curl \
        dhclient \
        dnsutils \
        gcc \
        git \
        gnupg \
        htop \
        make \
        mount \
        net-tools \
        networkmanager \
        pacman-contrib \
        rsync \
        openssh \
        sudo \
        tar \
        tmux \
        tree \
        unzip \
        vim \
        vpnc \
        wireguard-tools \
        zip

    echo "... DONE... cleaning up\n\n"

    # $SUDO ip link add dev wg0 type wireguard
    $SUDO systemctl start NetworkManager.service
    $SUDO systemctl enable NetworkManager.service
}


no_suspend() {
    # https://wiki.debian.org/SystemdSuspendSedation
    $SUDO sed -i "s/HandleLidSwitch=.*/HandleLidSwitch=ignore/" /etc/systemd/logind.conf
    $SUDO sed -i "s/HandleLidSwitchDocked=.*/HandleLidSwitchDocked=ignore/" /etc/systemd/logind.conf
    $SUDO sed -i "s/IdleActionSec=.*/IdleActionSec=90min/" /etc/systemd/logind.conf

    # turn off screen blanking
    # https://www.raspberrypi.org/forums/viewtopic.php?f=66&t=18200&sid=135af53eb82496bc64f4c0eefbc86d2c&start=25
    # http://raspberrypi.stackexchange.com/questions/752/how-do-i-prevent-the-screen-from-going-blank
    xset s noblank

    $SUDO systemctl restart systemd-logind.service
}

install_nvidia_docker() {
    # get latest driver from https://www.nvidia.com/object/unix.html
    # https://github.com/NVIDIA/nvidia-docker/wiki/Installation-(version-2.0)#prerequisites
    curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | \
            sudo tee /etc/apt/sources.list.d/nvidia-docker.list
            sudo apt-get update

            # Install nvidia-docker2 and reload the Docker daemon configuration
            sudo apt-get install -y nvidia-docker2
            sudo pkill -SIGHUP dockerd
}

install_latex() {
    echo "- installing LaTeX.."
    pacman -Syu \
        evince \
        texlive \
        texlive-bibtex-extra \
        texlive-fonts-extra \
        texlive-fonts-recommended \
        texlive-lang-english \
        texlive-lang-german \
        texlive-latex-extra \
        texlive-latex-recommended \
        texlive-pictures \
        texlive-plain-generic
    echo "..done"
}

install_i3() {
    echo "- installing i3wm and some tools..."

    # check if CJK and other eastern letters are displayed correctly:
    # https://meta.wikimedia.org/wiki/List_of_Wikipedias

    # tlp: Advanced Linux Power Management
    # http://linrunner.de/en/tlp/docs/tlp-linux-advanced-power-management.html
    # deb http://repo.linrunner.de/debian sid main

    $SUDO pacman -Syu \
           alsa-utils \
           arandr \
           aspell \
           aspell-de \
           aspell-en \
           emacs \
           feh \
           firefox \
           fzf \
           gimp \
           i3 \
           i3lock \
           i3lock-fancy \
           i3status \
           keepassxc \
           libreoffice-fresh \
           libreoffice-fresh-de \
           libreoffice-fresh-en-gb \
           lightdm \
           obs-studio \
           openvpn \
           powerline-fonts \
           pulseaudio \
           pulseaudio-alsa \
           pulsemixer \
           rxvt-unicode \
           scrot \
           shotwell \
           speedcrunch \
           thunderbird \
           thunderbird-l10n-de \
           thunderbird-l10n-en-gb \
           ttf-font-awesome \
           ttf-dejavu \
           ttf-opensans \
           udiskie \
           vlc \
           xorg \
           xorg-xrandr \
           noto-fonts \
           syncthing


    $SUDO localectl set-x11-keymap de,de ctrl:nocaps

        # cups-browsed \
        # feh \
        # firmware-linux-nonfree \
        # firmware-realtek \
        # fswebcam \
        # libnotify-bin \
        # libnotify-dev \
        # network-manager-gnome \
        # network-manager-vpnc \
        # network-manager-vpnc-gnome \
        # notify-osd \
        # ntfs-3g \
        # nmap \
        # pulseaudio \
        # remmina \
        # rxvt-unicode-256color \
        # slim \
        # --no-install-recommends

#     cat <<-EOF > /usr/share/dbus-1/services/org.freedesktop.Notifications.service
#     [D-BUS Service]
#     Name=org.freedesktop.Notifications
#     Exec=/usr/lib/notification-daemon/notification-daemon
# EOF

    TARGET="/home/$USERNAME/.emacs.d"
    git clone https://github.com/syl20bnr/spacemacs "${TARGET}"
#    rm -rf "${TARGET}"/private/snippets
#    git clone https://github.com/AndreaCrotti/yasnippet-snippets "${TARGET}"/private/yas-snippets
#    ln -s "${TARGET}"/private/yas-snips/snippets "${TARGET}"/private/snippets/

    no_suspend

    echo "... setting capslock to control"
    sed -i "s/^XKBOPTIONS=.*/XKBOPTIONS=\"ctrl:nocaps\"/" /etc/default/keyboard

    TARGET="/home/$USERNAME/.emacs.d"
    git clone https://github.com/syl20bnr/spacemacs "${TARGET}"
    #rm -rf "${TARGET}"/private/snippets
    #git clone https://github.com/AndreaCrotti/yasnippet-snippets "${TARGET}"/private/yas-snips
    #ln -s "${TARGET}"/private/yas-snips/snippets "${TARGET}"/private/snippets/

}

link_userdirs() {

    mount_dir=$1
    target_mount="${mount_dir:-/mnt/docs/}"
    user_dirs=("Documents Downloads Music Pictures Videos")
    for DIR in $user_dirs
    do
        echo "removing and linking ${DIR}..."
        rm -rfv /home/$USERNAME/$DIR
        echo "link target: ${target_mount}/${DIR}"
        ln -vs ${target_mount}/${DIR} /home/${USERNAME}/${DIR}
    done

    projects_mount=$docs_mount
}

install_wine() {
    # edit /etc/pacman.conf
    # TODO: enable multilib + Include mirrorlist

    # if using NVidia gpu
    $SUDO pacman -Syu \
          lib32-alsa-plugins \
          lib32-gnutls \
          lib32-libpulse \
          lib32-nvidia-utils \
          lib32-openal \
          multilib \
          wine
}

install_vscodium() {
    # https://vscodium.com/

    pacman -Syu \
        codium \
        --no-install-recommends

    codium version
}

install_docker() {
    echo "installing docker binary Version $VERS ..."
    # https://github.com/tianon/cgroupfs-mount/blob/master/cgroupfs-mount

    pacman -Syu \
        docker \
        docker-compose

    $SUDO usermod -aG docker $USERNAME

    docker version
    docker info
}


install_nvidia() {
    echo "Prepare install for NVIDIA proprietary display driver"

    pacman -Syu \
           nvidia
}

install_virtualbox() {
    # https://wiki.debian.org/VirtualBox#Installation_of_non-free_edition
    pacman -Syu \
            apt-transport-https \
            ca-certificates \
            curl \
            gnupg2 \
            software-properties-common \
            --no-install-recommends

}


main() {
    local cmd=$1

    if [[ -z "$cmd" ]]; then
        echo "Usage: \n base (includes docker+compose)| desktop | server | dotfiles | vscode | latex | update-docker | go"
    fi

    case "$cmd" in
        base)
            # apt_sources buster
            base_applications
            ;;
        server)
            base_applications
            install_server_base
            install_docker
            install_compose
            ;;
        desktop)
            base_applications
            install_docker
            install_compose
            install_i3
            if [ -f "./get_private_stuff.sh" ]; then
                source get_private_stuff.sh
            fi
            ;;
        links)
            link_userdirs /mnt/docs/
            ;;
        latex)
            install_latex
            ;;
        vscode)
            install_vscodium
            ;;
        update-docker)
            install_docker
            install_compose
            ;;
        go)
            install_golang
            ;;
    esac
}

main "$@"
