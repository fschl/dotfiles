alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias rs='rsync --progress -vur '
alias up='sudo apt update && sudo apt upgrade'
alias upnodock='sudo apt-mark hold docker-ce docker-ce-cli && sudo apt update && sudo apt upgrade && sudo apt-mark unhold docker-ce docker-ce-cli'

alias o='xdg-open'
alias s='kitty +kitten ssh'
alias ssh='kitty +kitten ssh'
alias cat='bat'
# alias rg='ripgrep'
alias ll='eza -ahlF'
alias la='eza -Ah'
alias feh='feh -d --scale-down'
alias camshot='fswebcam -r 640x480 --jpeg 90 -F 3 -D 1 --no-banner'

alias ta='tmux attach'

alias sk='sh ~/git/home-infra/tools/goking.sh'
alias wgfsu='sudo wg-quick up fschl-vpn'
alias wgfsd='sudo wg-quick down fschl-vpn'

alias routes='ip -o routes show'
alias ips='ip -o addr show | grep -v br-'
alias ports="echo '... ss -tl add -n to see port numbers ;-)'; ss -tl"
alias cons="echo '... ss -t add -r to resolv hostnames ;-)'; ss -te"
alias socks="echo '... ss -xl add -r to resolv hostnames ;-)'; ss -xl"
alias libver="ldconfig -p | grep "

alias act='sudo systemctl -l -t service --state=active'
alias fai='sudo systemctl -l -t service --state=failed'

# basic docker shortcuts
alias docker='podman'
alias dps='docker container ls -a'
alias di='docker image ls'
alias dv='docker volume '
alias dvl='docker volume ls'
alias dip="docker container inspect -f '{{ .NetworkSettings.IPAddress }}'" # call with <container name> or ID
alias dih="docker container inspect -f '{{ .Config.Hostname }}'" # call with <container name> or ID
alias dstats="docker stats "'$(sudo docker ps -aq)' # shows stats thingy for all containers

alias drmsc="docker container prune"
#alias drmsc="docker container ls -a | grep Exited | awk '{print $1}' | xargs docker container rm" # removes stopped containers

alias drmdi="docker image prune"
#alias drmdi="docker image remove "'$(docker image ls -q -f dangling=true)' # removes non-tagged images

# docker swarm
alias dss='docker stack services '
alias dsd='docker stack deploy -c docker-compose.yml '
