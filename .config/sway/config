set $mod Mod4
# at some point check https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway

# Configure displays

# https://github.com/swaywm/sway/wiki#display-configuration
# swaymsg -t get_outputs
set $primary 'ASUSTek COMPUTER INC PA279CRV SALMSB001834'
# set $primary 'LG Electronics 24MB65 512NTHM7N489'
set $secondary 'LG Electronics 24MB65 512NTHM7N489'
# set $secondary 'BNQ BenQ GW2450H K8D03191SL0'
set $laptop eDP-1

output $primary pos 1200 135 scale 1
output $secondary pos 0 0 transform 90

# output * bg ~/.dotfiles/backgrounds/samuel-ferrara-uOi3lg8fGl4-unsplash.jpg fill
# output * bg ~/.dotfiles/backgrounds/scott-rodgerson-lRtzEExP0H8-unsplash.jpg fill

bindsym $mod+Shift+s exec ~/.bin/toggle-display $primary

# https://github.com/swaywm/sway/wiki#input-configuration
# localectl list-x11-keymap-layouts - gives you layouts (~100 on modern systems)
# localectl list-x11-keymap-variants de - gives you variants for this layout
input "5426:270:Razer_Razer_BlackWidow" {
    xkb_layout de
    xkb_options ctrl:nocaps
}

# The internal keyboard of my ThinkPad X13 has a ANSI german keyboard
# which uses the variant E2 and moves
# < to AltGr+2
# > to AltGr+3
# | to AltGr+^
input "1:1:AT_Translated_Set_2_keyboard" {
    xkb_layout de
    xkb_variant e2
    xkb_options ctrl:nocaps
}

# input "4617:8963:Keyboardio_Atreus" {
input "4617:8963:Keyboardio_Atreus_Keyboard" {
    xkb_layout de,de
    xkb_options ctrl:nocaps
}

font pango:Iosevka ss08 7

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# kill focused window
bindsym $mod+Shift+q kill
bindsym $mod+Shift+x exit
bindsym $mod+Shift+r reload
# bindsym $mod+r resize
bindsym $mod+r mode "resize"

bindsym $mod+Shift+Return exec kitty
# bindsym $mod+Shift+Return exec emacsclient -c --eval '(vterm)'

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# use vim keys for window movement
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move workspace
bindsym $mod+Mod1+h move workspace to output left
bindsym $mod+Mod1+l move workspace to output right
bindsym $mod+Mod1+Left move workspace to output left
bindsym $mod+Mod1+Right move workspace to output right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left 30 px
bindsym $mod+Shift+Down move down 30 px
bindsym $mod+Shift+Up move up 30 px
bindsym $mod+Shift+Right move right 30 px
bindsym $mod+Shift+h move left 30 px
bindsym $mod+Shift+j move down 30 px
bindsym $mod+Shift+k move up 30 px
bindsym $mod+Shift+l move right 30 px

# split in horizontal orientation
# bindsym $mod+s split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# toggle tiling / floating
bindsym $mod+Shift+f floating toggle

# toggle pinned floating window
bindsym $mod+Shift+p sticky toggle

# change focus between tiling / floating windows
bindsym $mod+Control+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# App launcher
bindsym $mod+space exec fuzzel -w 50 -x 8 -y 8 -r 3 -b 232635ff -t A6Accdff -s A6Accdff -S 232635ff -C c792eacc -m c792eacc -f "Iosevka ss08:weight=light:size=11" --icon-theme="Papirus-Dark"

# Set defaults
default_orientation horizontal
workspace_layout tabbed

# Configure gaps and borders
default_border pixel 1
gaps outer 2
gaps inner 4
# smart_borders off
hide_edge_borders --i3 none 

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1 "
set $ws2 "2 "
set $ws3 "3 "
set $ws4 "4 "
set $ws5 "5 "
# unlock-alt - keepassxc defaults here
set $ws8 "8 "
# linux - less often used terminals for background processes
set $ws9 "9 "
set $ws0 "chat"

# find application info with =swaymsg -t get_tree=
assign [app_id="firefox"] $ws1
# assign [class="Emacs"] $ws2
assign [title="^DebugScreen"] $ws3
assign [app_id="org.keepassxc.KeePassXC"] $ws8

# Move specific workspaces to outputs
workspace $ws1 output $primary $laptop
workspace $ws2 output $primary $laptop
workspace $ws3 output $secondary $laptop
workspace $ws4 output $secondary $laptop
workspace $ws5 output $secondary $laptop
workspace $ws8 output $laptop
workspace $ws9 output $laptop
workspace $ws0 output $laptop

# Switch to workspace
# bindsym $mod+grave workspace $ws0
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace 0

# Move focused container to workspace
# bindsym $mod+Shift+grave move container to workspace $ws0
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace 0

# Bind hotkeys for brightness and volume
bindsym --locked XF86MonBrightnessDown exec brightnessctl set 5%-
bindsym --locked XF86MonBrightnessUp exec brightnessctl set 5%+
bindsym --locked $mod+p exec playerctl play-pause

# resize window (you can also use the mouse for that)
mode "resize" {
    # These bindings trigger as soon as you enter the resize mode

    # Pressing left will shrink the window’s width.
    # Pressing right will grow the window’s width.
    # Pressing up will shrink the window’s height.
    # Pressing down will grow the window’s height.
    bindsym j resize shrink width 10 px or 10 ppt
    bindsym i resize grow height 10 px or 10 ppt
    bindsym k resize shrink height 10 px or 10 ppt
    bindsym l resize grow width 10 px or 10 ppt

    # same bindings, but for the arrow keys
    bindsym Left resize shrink width 10 px or 10 ppt
    bindsym Down resize grow height 10 px or 10 ppt
    bindsym Up resize shrink height 10 px or 10 ppt
    bindsym Right resize grow width 10 px or 10 ppt

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

# Screenshots
bindsym Print exec grimshot --notify save output
bindsym Alt+Print exec grimshot --notify save area

# Style the UI
client.focused #c792eacc #232635dd #ffffffff #ffffffff #c792eacc 
client.unfocused #c792eacc #1c1f2bef #ffffffff #ffffffff #c792eacc 

# Set up screen locking
set $lock 'swaylock -f -i ~/.dotfiles/backgrounds/samuel-ferrara-uOi3lg8fGl4-unsplash.jpg -s fill --font "Iosevka ss08" --indicator-idle-visible'
exec swayidle -w \
  timeout 900 $lock \
  timeout 960 'swaymsg "output * dpms off"' \
	resume 'swaymsg "output * dpms on"' \
  before-sleep $lock

bindsym Ctrl+Shift+L exec $lock
# Window rules
#for_window [title="Firefox — Sharing Indicator"] kill
for_window [title=".* - mpv$"] floating enable, sticky enable

# Autostart applications
exec waybar --config ~/.config/waybar/x13-fedora.config
# exec waybar --config ~/.config/waybar/$(hostname).config
exec mako --border-radius=2 --font="Iosevka ss08 10" --max-visible=5 --outer-margin=5 --margin=3 --background="#1c1f26" --border-color="#89AAEB" --border-size=1 --default-timeout=7000
exec nm-applet --indicator
exec udiskie -t
exec gammastep-indicator -l 50.8643:12.1124 -t 6500:3500 -b 1.0:0.7

exec swaymsg workspace $ws2 && emacs --daemon && emacsclient -c
exec firefox
exec keepassxc

# Set GNOME settings (requires "glib:bin" in Guix)
exec gsettings set org.gnome.desktop.interface gtk-theme "Matcha-dark-azul"
exec gsettings set org.gnome.desktop.interface icon-theme "Papirus-Dark"
exec gsettings set org.gnome.desktop.interface font-name "Iosevka Aile 11"

# Update DBUS activation records to ensure Flatpak apps work
exec dbus-update-activation-environment --systemd DISPLAY WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway