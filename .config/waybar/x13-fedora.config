{
    "layer": "bottom",
    "position": "bottom",
    "height": 32,

    // maybe take another look at e.g. https://gitlab.com/wef/dotfiles/-/blob/master/.config/waybar/config
    // https://github.com/Alexays/Waybar/wiki/Configuration
    // https://github.com/Alexays/Waybar/wiki/Examples

    "modules-left": ["sway/workspaces", "sway/mode", "sway/window" ],
    "modules-center": [ "custom/disk_root", "network"],
    "modules-right": [ "wireplumber", "custom/mail", "cpu", "battery", "tray", "clock" ],
    "sway/mode": {
        "format": " {}"
    },
    "sway/workspaces": {
        "format": "{name}",
        "disable-scroll": true
    },
    "sway/window": {
       "icon": true
    },
    "clock": {
        "format": "{:%b %d %H:%M KW%U}",
        "tooltip": false
    },
    "battery": {
        "format": " {time} ({capacity}%) {icon}",
        "format-alt": "{capacity}% {icon}",
        "format-icons": ["", "", "", "", ""],
        "format-charging": "{capacity}% ",
        "interval": 30,
        "states": {
            "warning": 25,
            "critical": 10
        },
        "tooltip": false
    },
    "custom/disk_root": {
	"format": "💽/{} ",
	"interval": 30,
	"exec": "df -h --output=avail / | tail -1 | tr -d ' '",
        "tooltip": true ,
        "tooltip-format": "{path} ({percentage_used}% of {total})"
    },

    "temperature": {
	// "thermal-zone": 2,
	// "hwmon-path": "/sys/class/hwmon/hwmon2/temp1_input",
	"critical-threshold": 80,
	// "format-critical": "{temperatureC}°C {icon}",
	"format": "<span color='#e88939'>{icon}</span> {temperatureC}°C",
	"format-icons": ["", "", ""]
    },
    "cpu": {
	"format": "🏭 {usage}%",
	"tooltip": false
    },
    "memory": {
	"format": "💾 {used:0.1f}G"
    },
    "custom/mail": {
	"format": "📩{}",
	"interval": 30,
	"exec": "notmuch count tag:flagged OR tag:inbox AND NOT tag:killed"
    },
    "network": {
	// "family": "ipv6",
	"format-wifi": "<span color='#589df6'></span> <span color='gray'>{essid}</span> <span color='#589df6'>⇵</span> {bandwidthUpBits}/{bandwidthDownBits}",
	"tooltip-format-wifi": "Freq: {frequency} <span color='#589df6'>{signaldBm} dB</span>",
	"format-ethernet": "{ifname}: {ipaddr}/{cidr} ",
	"format-linked": "{ifname} (No IP) ",
	"format-disconnected": "Disconnected ⚠",
	"format-alt": "{ifname}: {ipaddr}/{cidr}",
	"interval": 5
    },
    "wireplumber": {
        "format": "{icon}{volume}%",
        "format-muted": "MUTED",
        "on-click": "helvum",
        "on-click-right": "wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle",
        "format-icons": ["", "", ""]
    },
    //"timer": {
    //    "interval": 1,
    //    "exec": "~/.config/waybar/timer.sh",
    //    "exec-if": "pgrep emacs",
    //    "escape": true
    //},
    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        },
        "tooltip": false
    },
    "tray": {
        "icon-size": 18,
        "show-passive-items": true
    },
    "cpu": {
      "interval": 2,
      "format": "{icon0}{icon1}{icon2}{icon3}{icon4}{icon5}{icon6}{icon7}",
      "format-icons": ["▁", "▂", "▃", "▄", "▅", "▆", "▇", "█"],
    },
}
